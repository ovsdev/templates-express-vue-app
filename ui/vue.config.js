const path = require('path')

module.exports = {
  devServer: {
    disableHostCheck: true,
    proxy: {
      "/api/": {
        target: 'http://localhost:3001'
      }
    }
  }
}
