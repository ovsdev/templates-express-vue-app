import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios, { AxiosStatic } from 'axios'
import { BootstrapVue, IconsPlugin, PaginationPlugin , TooltipPlugin} from 'bootstrap-vue'
const moment = require('moment')
require('moment/locale/it')

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(PaginationPlugin)
Vue.use(TooltipPlugin)

Vue.use(require('vue-moment'), { moment });

Vue.config.productionTip = false

// Make Axios available globally
Vue.prototype.$axios = axios

declare module 'vue/types/vue' {
  interface Vue {
    $axios: AxiosStatic
  }
}

// Install Bootstrap
import 'bootstrap/dist/css/bootstrap.css'

// Main app component
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
