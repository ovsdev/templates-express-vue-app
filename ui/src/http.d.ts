// In TypeScript, it's necessary to augment the Vue module to make
// the $http property (added by Axios) visible
// See: https://vuejs.org/v2/guide/typescript.html#Augmenting-Types-for-Use-with-Plugins
// import Vue from 'vue'

// declare module 'vue/types/vue' {
//   interface Vue {
//     $http: any
//   }
// }
