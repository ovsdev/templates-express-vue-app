# templates-express-vue-app
This is a template of an application based on bff (Backend for fronted) pattern

## Template structure
The application is composed by two main parts:

1. server:      Backend of the application built with an Express server
2. ui:          Frontend part of the application built with a Vue js UI
3. chart:       Helm chart of the application
3. build.sh:    Build script used in cicd

In this example the backend receive request from the frontend and forward the request to an external service (in this example sc-data-catalog-api) delpoyed in the cluster.

## Project setup 
In the development phase you can run separately server and ui for dev and debugging porpouse 

### Run server
open a new terminal in the devpod

```
cd ./server

npm install

npm run dev   
```
N.B During the dev phase we can run the server on a port different from 3000 (ex: 3001) so that we can run the UI on the 3000 and access the ui preview


### Run ui
open a new terminal in the devpod

```
cd ./ui

npm install

npm run serve   
```

In the ./ui/vue.config.js we can configure the vue devServer in order to forward all request from ui to our runnig server on 3001

```
module.exports = {
  devServer: {
    disableHostCheck: true,
    proxy: {
      "/api/": {
        target: 'http://localhost:3001'
      }
    }
  }
}
```


### Compiles and minifies UI for production
``` 
cd ./ui

npm run build  
```

## Build

For this project we have to prepare a custom build.sh script that will be executed during the cicd phases<br>

We included the build steps of the ui and server and the command for the docker build step. <br>

N.B If the build require to produce a docker image for the deploy, use the instruction as in the example<br>
``` 
docker build --network=host -t $DOCKER_IMAGE_TAG . || exit 1
``` 

