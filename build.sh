#!/bin/bash
## This is a custom build script executed during cicd build
## This approach is necessary in case the application is composed of more then one project
## In this example there is a server part built with Express and a ui part built with Vue   

set -x
 
## build the ui vue application
cd ./ui
npm install
npm run build || exit 1

## build the express server app
cd ../server

npm install

npm run build || exit 1

## move the vue build artifacts into the exposed folder of the server app
cd ./dist
rm -fr ./public
UI_HOME=../../ui
UI_DIST="$UI_HOME/dist"

mkdir public
mkdir public/js
mkdir public/css
mkdir public/img

cp -rf "$UI_DIST/js/"*.js public/js
cp -rf "$UI_DIST/css/"*.css public/css
cp -rf "$UI_DIST/img/"* public/img
cp -rf "$UI_DIST/index.html" public
cp -rf "$UI_DIST/favicon.ico" public


## build the application docker image
## A gloabal variable DOCKER_IMAGE_TAG is setup during the build process, use this value to build the app docker image   
## NOTE: remember to use the --network=host while building the docker image
cd ..
docker build --network=host -t $DOCKER_IMAGE_TAG . || exit 1