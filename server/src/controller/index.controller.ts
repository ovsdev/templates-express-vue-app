import * as express from 'express'
import { jsonResponse, errorResponse } from '../utils'
import IndexModel from '../model/index.model'

class IndexController {
    
    // implement here your controller logics

    static async find(req: express.Request, res: express.Response) {

        try {
            let model = new IndexModel()
            let result = model.getModel()
            jsonResponse(res, result);
        } catch (error) {
            errorResponse(res, error)
        }
        

    }

}

export default IndexController