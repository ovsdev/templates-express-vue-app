import express from "express";
import cookieParser from "cookie-parser";
import logger from "morgan";
import path from 'path';
let proxy = require('express-http-proxy');

const app = express();

app.use(logger('tiny'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

import indexRouter from './routes/index';

app.use('/api/index',indexRouter);

// start the Express server
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});

module.exports = app;
