import express from 'express';
const router = express.Router();
let proxy = require('express-http-proxy');

import IndexController from '../controller/index.controller';

const EXTERNAL_SERVICE = 'https://<your-external-service>'

// define a specific controller method for a path
router.get("/", IndexController.find);

// here you can forward other requests to an external service
router.all('*', proxy(`${EXTERNAL_SERVICE}`));

export default router