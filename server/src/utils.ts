import { StatusCodes } from 'http-status-codes';
import * as express from 'express'

export function formatError(err: any) {
    return { error: err }
}

export function formatData(result: any) {
    return { data: result }
}

export function jsonResponse(res: express.Response, data: any) {
    res.status(StatusCodes.OK).json(formatData(data));
}

export function jsonCreatedResponse(res: express.Response) {
    res.sendStatus(StatusCodes.CREATED);
}

export function errorResponse(res: express.Response, error: any) {
    console.error(error)
    let errorMsg = error.message || error
    res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(formatError(errorMsg));
}

export function unauthorizedResponse(res: express.Response) {
    res.sendStatus(StatusCodes.UNAUTHORIZED);
}
